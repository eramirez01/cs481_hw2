import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column (
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('This is Cal State San Marcos', style: TextStyle(
                    fontWeight: FontWeight.bold,),
                  ),
                ),
                Text('Pretty big school that is empty right now.', style: TextStyle(
                  color: Colors.grey[500],),
                ),
              ],
            ),
          ),
          Icon(Icons.home, color: Colors.red[500],),
          Text('11'),
        ],
      ),
    );

    Color color = Theme.of(context).primaryColor;
    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children:[
          _buildButtonColumn(color, Icons.school, 'School'),
          _buildButtonColumn(color, Icons.directions_walk, 'Stairs'),
          _buildButtonColumn(color, Icons.gamepad, 'Break Room'),
        ],
      ),
    );



    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'This school has always had so many stairs just to get to class'
            'Back then, I detested going through so many stairs'
            'But now that we cannot go to school cause of the pandemic'
            'It makes me appreciate those times that much more'
            'It was not all bad going to school and I hope to come back after this is all over'
            'Just to walk around the campus and to walk the stairs',
        softWrap: true,
      ),
    );

//Widget textSection = Container(...);
    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Layout Homework'),
        ),
        body: ListView(
            children: [
              Image.asset(
                'School.jpg',
                width: 600,
                height: 240,
                fit: BoxFit.cover,
              ),
              titleSection,
              buttonSection,
              textSection
            ]
        ),
      ),
    ); //MaterialApp



  }


} //MyApp
Column _buildButtonColumn(Color color, IconData icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Icon(icon, color: color),
      Container(
        margin: const EdgeInsets.only(top: 8),
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      ),
    ],
  );
}

